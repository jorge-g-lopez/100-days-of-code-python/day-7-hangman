""" Hangman """

import random

stages = [
    """
  +---+
  |   |
  O   |
 /|\  |
 / \  |
      |
=========
""",
    """
  +---+
  |   |
  O   |
 /|\  |
 /    |
      |
=========
""",
    """
  +---+
  |   |
  O   |
 /|\  |
      |
      |
=========
""",
    """
  +---+
  |   |
  O   |
 /|   |
      |
      |
=========""",
    """
  +---+
  |   |
  O   |
  |   |
      |
      |
=========
""",
    """
  +---+
  |   |
  O   |
      |
      |
      |
=========
""",
    """
  +---+
  |   |
      |
      |
      |
      |
=========
""",
]

word_list = ["ardvark", "baboon", "camel"]

chosen_word = random.choice(word_list)

display = []
for letter in chosen_word:
    display += "_"

lives = 6
end_of_game = False

while not end_of_game:
    guessed = False
    guess = input("Guess a letter: ").lower()

    if guess in display:
        print(f"You have already guessed {guess}")
        guessed = True
    else:
        for i in range(len(chosen_word)):
            if chosen_word[i] == guess:
                display[i] = guess
                guessed = True

    print(f"{' '.join(display)}")

    if "_" not in display:
        end_of_game = True
        print("You win!")

    if not guessed:
        lives -= 1
        print(f"You guessed {guess}, that's not in the word. You lose a life.")
        print(stages[lives])

    if lives == 0:
        end_of_game = True
        print("You lose.")
